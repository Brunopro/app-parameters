import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  //Assim que a página é carregada o ionic chama esse método automaticamente
  ionViewDidLoad() {
    //aqui estamos imprimindo os parametros enviados da home para feed,
    // se clicarmos no pushFeed da home receberemos um tipo de parametros
    //se clicarmos no setFeed receberemos outros tipo
    console.log(this.navParams); //lista todos os parametros
    console.log(this.navParams.get('type')) //vai retornar o valor do tipo type
    this.navParams.data.type; // retorna o valor do type, essa sintaxe é conceito de javascript

    //duas formas de chamar o metodo message
    this.navParams.data.message();
    //this.navParams.data['message']();




  }

}
