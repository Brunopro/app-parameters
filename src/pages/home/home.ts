import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { FeedPage } from '../feed/feed';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  pushPage() :void {
  //o push consegue obter uma promisse exemplo, this.navCtrl.push().then()...
  this.navCtrl.push(FeedPage, {
    type: 'push',
    course: 'ionic 2 and 3, teatcher plinio naves',
    year: 2017,
    message: () => {
      console.log('Welcome to ionic 2 and 3 course!!');

    }
  });
  }

  setPage() :void {
    this.navCtrl.setRoot(FeedPage, {
      type: 'set'

    });
  }

}
